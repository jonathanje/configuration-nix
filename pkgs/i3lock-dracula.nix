{ writeShellScriptBin }:
writeShellScriptBin "i3lock-dracula" ''
  i3lock-color \
  --clock \
  --insidevercolor=00000000 \
  --insidewrongcolor=00000000 \
  --insidecolor=00000000 \
  --ringvercolor=00000000 \
  --ringwrongcolor=00000000 \
  --ringcolor=00000000 \
  --linecolor=00000000 \
  --keyhlcolor=bd93f9ff \
  --bshlcolor=ff79c6ff \
  --separatorcolor=00000000 \
  --verifcolor=ffffffff \
  --wrongcolor=ffffffff \
  --timecolor=f8f8f2ff \
  --timestr="%H:%M" \
  --datecolor=f8f8f2ff \
  --datestr="%a %d-%b-%Y %R" \
  --time-font="Helvetica Neue Lt Std" \
  --date-font="Helvetica Neue Lt Std" \
  --wrong-font="Source Code Pro" \
  --layout-font="Source Code Pro" \
  --verif-font="Source Code Pro" \
  --timepos="960:145" \
  --datepos="960:200" \
  --timesize=64 \
  --datesize=32 \
  --veriftext="CHECK" \
  --wrongtext="WRONG" \
  --noinputtext="EMPTY" \
  --locktext="LOCK" \
  --lockfailedtext="FAIL" \
  --radius=260 \
  --ring-width=2 \
  --image=/home/jeppener/Bilder/Wallpaper/i3lock.png
''
