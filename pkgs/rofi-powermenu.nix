{ writeShellScriptBin }:
writeShellScriptBin "rofi-powermenu" ''

# Options
shutdown="SHUTDOWN"
reboot="REBOOT"
lock="LOCK"
suspend="SUSPEND"
logout="LOGOUT"

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"

chosen="$(echo -e "$options" | rofi -p "" -dmenu -selected-row 2)"
case $chosen in
    $shutdown)
        systemctl poweroff
        ;;
    $reboot)
        systemctl reboot
        ;;
    $lock)
        i3lock-dracula
        ;;
    $suspend)
        mpc -q pause
        amixer set Master mute
        systemctl suspend
        ;;
    $logout)
        kill -9 -1
        ;;
esac
''
