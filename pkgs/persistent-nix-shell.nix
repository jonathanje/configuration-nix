# adapted from https://code.kummerlaender.eu/pkgs/tree/pkgs/persistent-nix-shell
{ writeShellScriptBin }:
writeShellScriptBin "persistent-nix-shell" ''
  mkdir -p .gcroots

    inherit_shell=""
    update=""

    while getopts 'su' flag; do
          case "''${flag}" in
                s) inherit_shell='true' ;;
                u) update='true' ;;
          esac
    done

    if [[ ! -f "$PWD/.gcroots/shell.drv" || $update ]]; then
                nix-instantiate shell.nix --indirect --add-root $PWD/.gcroots/shell.drv > /dev/null
                nix-store -r $(nix-store --query --references $PWD/.gcroots/shell.drv) --indirect --add-root $PWD/.gcroots/shell.dep > /dev/null
    fi

    # Fix to prevent implicit interactiveBash dependency
    if [[ -f "/bin/bash" ]]; then
                export NIX_BUILD_SHELL=/bin/bash
    else
                export NIX_BUILD_SHELL=/run/current-system/sw/bin/bash
    fi


    if [ $inherit_shell ]; then
                exec nix-shell $(readlink $PWD/.gcroots/shell.drv) --command $SHELL
    else
                exec nix-shell $(readlink $PWD/.gcroots/shell.drv)
    fi
      ''
