{ writeShellScriptBin }:
writeShellScriptBin "toolkit-rofi" ''
  #Options
  poweroff = "poweroff    ⏻"
  reboot =   "reboot      ↺"
  lock =     "lock        "
  suspend=   "suspend     ⏾"
  logout =   "gdm         "

  options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"
  chosen="$(echo -e "$options" | rofi -p -dmenu )"
case $chosen in
    $shutdown)
        systemctl poweroff
        ;;
    $reboot)
        systemctl reboot
        ;;
    $lock)
        i3lock
        ;;
    $suspend)
        mpc -q pause
        amixer set Master mute
        systemctl suspend
        ;;
    $logout)
        openbox --exit
        ;;
  esac

''
