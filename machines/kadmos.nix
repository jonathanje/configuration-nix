# Edit this configuration file to define what should be installed on your system.  Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’).  
{ config, pkgs, ... }: { imports = [ # Include the results of the hardware scan.  
./hardware-kadmos.nix
./kadmos-secret.nix]; 

#Use the systemd-boot EFI boot loader.  
  boot.loader.grub = { 
    enable = true; 
    version = 2; 
    device = "nodev";
    efiSupport = true;
  };
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "kadmos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  networking.interfaces.eno1.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
environment.systemPackages = with pkgs; [
  wget 
  vim 
  home-manager 
  git
  figlet
  scrot
  lolcat
  kitty
  git-crypt
  virtualgl
  python37
  ];
environment.etc."/figlet/roman.flf".source = builtins.fetchurl {
  url = http://www.figlet.org/fonts/roman.flf;
  sha256 = "616101ac8fb911d8f0f800b61c86536e1849abc4d7324760383e76c45c344817";
};
networking.networkmanager.enable = true;
fonts.fonts = with pkgs; [
  source-code-pro
  font-awesome_4
];
services.xserver = {
  enable = true;
  layout = "de";
  xkbOptions = "caps:escape";
};

# Set your time zone.
time.timeZone = "Europe/Berlin";
i18n = {
  consoleKeyMap = "de";
  defaultLocale = "de_DE.UTF-8";
};

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
   services.openssh.enable = true;
   services.openssh.passwordAuthentication = false;
   services.openssh.permitRootLogin = "no";
   services.openssh.extraConfig = ''
     Banner /etc/ssh/sshd_banner
     '';
   services.sshd.enable = true;

   environment.etc."/ssh/sshd_banner".text = ''
     ********************************************************************************
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              * 
     *     oooo                        .o8                                          *
     *     `888                       "888                                          *
     *      888  oooo   .oooo.    .oooo888  ooo. .oo.  .oo.    .ooooo.   .oooo.o    *
     *      888 .8P'   `P  )88b  d88' `888  `888P"Y88bP"Y88b  d88' `88b d88(  "8    *
     *      888888.     .oP"888  888   888   888   888   888  888   888 `"Y88b.     *
     *      888 `88b.  d8(  888  888   888   888   888   888  888   888 o.  )88b    *
     *     o888o o888o `Y888""8o `Y8bod88P" o888o o888o o888o `Y8bod8P' 8""888P'    *
     *                                                                              *
     *     useae@student.kit.edu                Lattice Boltzmann Research Group    *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     *                                                                              *
     ********************************************************************************
     '';

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
   services.printing.enable = true;
   services.printing.drivers = [ pkgs.brgenml1cupswrapper ];
   services.avahi.enable = true;
   services.avahi.nssmdns = true;

  # Enable sound.
   sound.enable = true;
   hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.jeppener = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
   };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

  boot.initrd.luks.devices = [
    { 
    name = "root";
    device = "/dev/disk/by-uuid/0ce62ae1-ad9c-4142-81de-72b022cab969";
    preLVM = true;
    allowDiscards = true;
    }
  ];

  hardware.cpu.intel.updateMicrocode = true;

}

