{ config, pkgs, lib, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/installer/virtualbox-demo.nix> 
  ];
services.xserver = {
  desktopManager.plasma5.enable = lib.mkForce false;
  displayManager = {
    sddm.enable = lib.mkForce false;
  };
};
}
