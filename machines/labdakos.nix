# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let
	unstable = import <nixpkgs-unstable> {};
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-labdakos.nix
      ./labdakos-secret.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 15;
  boot.loader.efi.canTouchEfiVariables = true;
 # boot.kernelParams = [ "usbcore.quirks=0bda:8153:k" "acpi_rev_override" ]; 
  boot.kernelParams = [ "acpi_rev_override" ]; 
  boot.kernelPackages = unstable.linuxPackages_5_6;

  hardware.enableRedistributableFirmware = true;
  services.thermald.enable = lib.mkDefault true;
  
  powerManagement.cpuFreqGovernor =
    lib.mkIf config.services.tlp.enable (lib.mkForce null);
  services.tlp.enable = true;

  boot.initrd.luks.devices = {
      root = { 
      device = "/dev/disk/by-uuid/66a859a5-779d-41e5-b1b1-f01ddc2cba9f";
      preLVM = true;
      allowDiscards = true;
	};
    };
                  

  networking.hostName = "labdakos"; # Define your hostname.
  networking.wireless.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
     console.earlySetup = true;
     console.font = "Lat2-Terminus16";
     console.keyMap = "de";
   i18n = {
     defaultLocale = "en_US.UTF-8";
   };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
   environment.systemPackages = with pkgs; [
     wget 
     vim
     firefox
     git
     feh
     pypi2nix
     input-utils
     texlive.combined.scheme-full
   ];

   programs.light.enable = true;
  services.actkbd = {
    enable = true;
    bindings = [
      { keys = [ 224 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -U 10"; }
      { keys = [ 225 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -A 10"; }
    ];
  };

 #  nixpkgs.overlays = [
 #    (self: super: 
 #    let
 #     iwlib = self.python37Packages.buildPythonPackage rec {
 #       pname = "iwlib";
 #       version = "1.7.0";

 #       src = pkgs.python37.pkgs.fetchPypi {
 #         inherit pname version;
 #         sha256 = "18bd35wn7zclalpqbry42pf7bjrdggxkkw58mc0k1vkhg9czc1d8";
 #       };
 #       doCheck = false;
 #       propagatedBuildInputs = [ self.python37Packages.cffi self.wirelesstools ];
 #     };
 #    in{
 #      qtile = (super.qtile.overrideAttrs (oldAttrs: rec {
 #        name = "my-qtile";
 #        buildInputs = oldAttrs.buildInputs ++ [ iwlib ];
 #        propagetedBuildInputs = [ iwlib ];
 #      }));
 #      })
 #  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  #   pinentryFlavor = "gnome3";
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
   services.openssh.enable = true;
   services.openssh.passwordAuthentication = false;
   services.openssh.permitRootLogin = "no";
   services.openssh.extraConfig = ''
     Banner /etc/ssh/sshd_banner
     '';
   services.sshd.enable = true;

   environment.etc."/ssh/sshd_banner".text = ''
     ***********************************************************************************
     *                                                                             	*      
     *                                                                             	 *     
     *                                                                                    *    
     *                                                                                     *   
     *                                                                                      *  
     *                                                                                       * 
     *                                                                                       *
     * o888             oooo               oooo            oooo                              * 
     *  888   ooooooo    888ooooo     ooooo888   ooooooo    888  ooooo ooooooo    oooooooo8  * 
     *  888   ooooo888   888    888 888    888   ooooo888   888o888  888     888 888ooooooo  * 
     *  888 888    888   888    888 888    888 888    888   8888 88o 888     888         888 * 
     * o888o 88ooo88 8o o888ooo88     88ooo888o 88ooo88 8o o888o o888o 88ooo88   88oooooo88  * 
     *                                                                                       *
     * jobojeha@jeppener.de                                     Jonathan Jeppener-Haltenhoff *
     *                                                                                       * 
     *                                                                                      *
     *                                                                                     * 
     *                                                                                    * 
     *                                                                                   * 
     *                                                                                  * 
     ***********************************************************************************
     '';

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
   services.printing.enable = true;
   services.printing.drivers = [ pkgs.brgenml1cupswrapper pkgs.hplip pkgs.brlaser ];
   services.avahi.enable = true;
   services.avahi.nssmdns = true;

  # Enable sound.
   sound.enable = true;
   hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "de";
  services.xserver.xkbOptions = "caps:escape";

  fonts.fonts = with pkgs; [
    source-code-pro
    font-awesome_4
    comfortaa
 #   nerdfonts
 #   helvetica-neue-lt-std
 #   (
 #     pkgs.fetchFromGitHub {
 #       name = "rofi-feather-font";
 #       owner = "adi1090x";
 #       repo = "rofi";
 #       rev = "30d07a684d4fa17a317a997bfb662869e071bce8";

#        postFetch = ''
#          tar xf $downloadedFile --strip=1
#          install -Dm444 -t $out/share/fonts/truetype/ fonts/feather.ttf
#          '';
#          sha256 = "1i6g7mb4hza1zkzhwqrzfgzhnnrsj5cf5jp4pbhghir5b0by4gnl";
#        }
#    )
  ];

   services.xserver.displayManager = {
     lightdm = {
       enable = true;
       greeter.enable = true;
       autoLogin.enable = true;
       autoLogin.user = "jeppener";
     };
     defaultSession = "home-manager";
   };

   services.xserver.desktopManager.session = [
     {
       name = "home-manager";
       start = ''
         ${pkgs.runtimeShell} $HOME/.hm-xsession &
         waitPID=$!
         '';
       }
     ];

  # Enable touchpad support.
   services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.jeppener = {
     isNormalUser = true;
     extraGroups = [ "video" "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
   };
  hardware.cpu.intel.updateMicrocode = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

}

