{ config, pkgs, lib, ... }:

{
    imports = [
      ./home/packages.nix
      ./home/vim.nix
      ./home/active-home.nix
    ];
}

