# CONFIGURATION.NIX

{ config, pkgs, lib, ... }:

{
  imports = [ 
    ./active-host.nix
    <home-manager/nixos>
  ];

  home-manager.users.jeppener = { config, pkgs, ... }: {
    imports = [
      ./home.nix
    ];
  };

nixpkgs.config.allowUnfree = true;
boot.supportedFilesystems = [ "ntfs" ];

environment = {
	sessionVariables.TERMINAL = [ "kitty" ];
	systemPackages = with pkgs; [
		wget
		vim
		home-manager
		git
		kitty
		git-crypt
		python37
	];
};
		

# Keep NixOS up-to-date automatically (systemctl list-timers)
system.autoUpgrade.enable = true;

}

