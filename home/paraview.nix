{ config, pkgs, ... }:

let
	nixpkgs-unstable = import <nixpkgs-unstable> {};

	python-packages-for-paraview = python-packages: with python-packages; [
		numpy
		matplotlib
		scipy
	];
	python-for-paraview = (
		pkgs.python2.withPackages python-packages-for-paraview
	).override { 
		ignoreCollisions = true; 
	};
in
{
	home.packages = [
		(pkgs.runCommand "paraview+" {
			buildInputs = [
				pkgs.makeWrapper
				nixpkgs-unstable.paraview
			];} ''
				mkdir $out
				ln -s ${nixpkgs-unstable.paraview}/* $out
				rm $out/bin
				mkdir $out/bin
				ln -s ${nixpkgs-unstable.paraview}/bin/* $out/bin
				rm $out/bin/pvbatch
				rm $out/bin/paraview
				rm $out/bin/pvpython
				makeWrapper ${nixpkgs-unstable.paraview}/bin/pvbatch $out/bin/pvbatch+ \
					--set PYTHONPATH "${python-for-paraview}/${pkgs.python.sitePackages}"
				makeWrapper ${nixpkgs-unstable.paraview}/bin/paraview $out/bin/paraview+ \
					--set PYTHONPATH "${python-for-paraview}/${pkgs.python.sitePackages}"
				makeWrapper ${nixpkgs-unstable.paraview}/bin/pvpython $out/bin/pvpython+ \
					--set PYTHONPATH "${python-for-paraview}/${pkgs.python.sitePackages}"
			'')
	];
				

}
	
	
