{ config, pkgs, stdenv, ... }:

with pkgs;
  let
    pkgs-unstable = import <nixos-unstable> {};
    dracula-background = "#282a36";
    dracula-currentLine= "#44475a";
    dracula-foreground= "#f8f8f2";
    dracula-comment= "#6272a4";
    dracula-cyan= "#8be9fd";
    dracula-green= "#50fa7b";
    dracula-orange= "#ffb86c";
    dracula-pink= "#ff79c6";
    dracula-purple= "#bd93f9";
    dracula-red= "#ff5555";
    dracula-yellow= "#f1fa8c";
  in
{
  imports = [
    ./labdakos-secret.nix
  ];

  nixpkgs.config.allowUnfree = true;
  home.packages = [ 
    zathura
    blender
    gimp
    onedrive
    bitwarden
    skypeforlinux
    i3status-rust
    openvpn
    neofetch
    libreoffice
    acpi
    i3lock
    i3lock-color
    xorg.xdpyinfo
    hugo
    filezilla
    thunderbird
    qemu
    (pkgs.callPackage ../../pkgs/persistent-nix-shell.nix {})
    (pkgs.callPackage ../../pkgs/rofi-powermenu.nix {})
    (pkgs.callPackage ../../pkgs/i3lock-dracula.nix {})
  ];

programs.rofi = {
  enable = true;
  padding = 500;
  width = 500;
  font = "Source Code Pro 16";
  scrollbar = false;
  separator = "none";
  cycle = true;
  colors = {
                 window = {
                   background = dracula-background;
                   border = dracula-comment;
                   separator = dracula-comment;
                 };

                 rows = {
                   normal = {
                     background = dracula-background;
                     foreground = dracula-foreground;
                     backgroundAlt = dracula-background;
                     highlight = {
                       background = dracula-background;
                       foreground = dracula-purple;
                     };
                   };
                 };
               };

  fullscreen = true;
};

xsession = {
  enable = true;

  windowManager.i3 = {
    enable = true;
    package = pkgs.i3-gaps;
    config = {

      modifier = "Mod4";
      fonts = [ "Source Code Pro 11" ];
      keybindings = lib.mkOptionDefault {

        "${config.xsession.windowManager.i3.config.modifier}+d" = "exec ${pkgs.rofi}/bin/rofi -show drun -display-run '' -no-show-match";
        "${config.xsession.windowManager.i3.config.modifier}+Return" = "exec ${pkgs.kitty}/bin/kitty";
        "Control+Mod1+l" = "exec rofi-powermenu";
        "XF86AudioRaiseVolume" = "exec --no-startup-id pactl set-sink-volume 0 +5%";
        "XF86AudioLowerVolume" = "exec --no-startup-id pactl set-sink-volume 0 -5%";
        "XF86AudioMute" = "exec --no-startup-id pactl set-sink-mute 0 toggle";
        "XF86MonBrightnessUp" = "exec --no-startup-id brightnessctl -s +10%";
        "XF86MonBrightnessDown" = "exec --no-startup-id brightnessctl -s -10%";
      };
      colors = {
        background = dracula-background;
        focused = {
          background = dracula-background;
          border = dracula-comment;
          childBorder = dracula-comment;
          indicator = dracula-comment;
          text = dracula-foreground;
        };
        focusedInactive = {
          background = dracula-background;
          border = dracula-currentLine;
          childBorder = dracula-background;
          indicator = dracula-purple;
          text = dracula-foreground;
        };
        placeholder = {
          background = dracula-background;
          border = dracula-currentLine;
          childBorder = dracula-background;
          indicator = dracula-purple;
          text = dracula-foreground;
        };
        unfocused = {
          background = dracula-background;
          border = dracula-currentLine;
          childBorder = dracula-background;
          indicator = dracula-comment;
          text = dracula-foreground;
        };
        urgent = {
          background = dracula-background;
          border = dracula-currentLine;
          childBorder = dracula-background;
          indicator = dracula-red;
          text = dracula-foreground;
        };
      };
      gaps = {
        inner = 10;
        smartBorders = "on";
        smartGaps = true;

      };
      bars = [{ 
          fonts = [ "Source Code Pro 11" ]; 
          statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ${config.xdg.configHome}/i3/status.toml"; 
          colors = {
            background = dracula-background;
            statusline = dracula-foreground; 
            separator = dracula-currentLine;
            focusedWorkspace = {
              background = dracula-currentLine;
              border = dracula-currentLine;
              text = dracula-foreground;
            };
            activeWorkspace = {
              background = dracula-background;
              border = dracula-currentLine;
              text = dracula-foreground;
            };
            bindingMode = {
              background = dracula-red;
              border = dracula-red;
              text = dracula-foreground;
            };
            inactiveWorkspace = {
              background = "dracula-background";
              border = dracula-background;
              text = "dracula-purple";
            };
            urgentWorkspace = {
              background = dracula-red;
              border = dracula-background;
              text = dracula-foreground;
            };
          };
      }];
      startup = [
        { command = "setxkbmap -layout de"; }
        { command = "feh --bg-scale /home/jeppener/Bilder/Wallpaper/nix-wallpaper.png"; }
      ];
    };
  };
};

xsession.scriptPath = ".hm-xsession";

xdg.configFile."kitty/kitty.conf".text = ''
# https://draculatheme.com/kitty
#
# Installation instructions:
#
#  cp dracula.conf ~/.config/kitty/
#  echo "include dracula.conf" >> ~/.config/kitty/kitty.conf
#
# Then reload kitty for the config to take affect.
# Alternatively copy paste below directly into kitty.conf
foreground ${dracula-foreground}
background ${dracula-background}
selection_foreground ${dracula-background}
selection_background ${dracula-foreground}
color0     #000000
color8     #4D4D4D
color1     #FF5555
color9     #FF6E67
color2     #50FA7B
color10    #5AF78E
color3     #F1FA8C
color11    #F4F99D
color4     #BD93F9
color12    #CAA9FA
color5     #FF79C6
color13    #FF92D0
color6     #8BE9FD
color14    #9AEDFE
color7     #BFBFBF
color15    #E6E6E6

# URL styles
url_color #BD93F9
url_style single

# Cursor styles
cursor #E6E6E6

# Tab bar colors and styles
tab_fade 1
active_tab_foreground ${dracula-background}
active_tab_background ${dracula-foreground}
active_tab_font_style bold
inactive_tab_foreground ${dracula-foreground}
inactive_tab_background ${dracula-background}
inactive_tab_font_style normal
'';

xdg.configFile."i3/status.toml".text = ''
    [theme]
    name = "plain"
    [theme.overrides]
    idle_bg = "${dracula-background}"
    idle_fg = "${dracula-foreground}"
    info_bg = "${dracula-background}"
    info_fg = "${dracula-purple}"
    good_bg = "${dracula-background}"
    good_fg = "${dracula-green}"
    warning_bg = "${dracula-background}"
    warning_fg = "${dracula-pink}"
    critical_bg = "${dracula-red}"
    critical_fg = "${dracula-background}"
    separator = ' '
    separator_bg = "${dracula-background}"
    separator_fg = "${dracula-foreground}"

    [icons] 
    name = "none"

    [[block]]
    block = "cpu"
    interval = 1
    format = "{utilization}%"

    [[block]]
    block = "net"
    device = "wlp0s20f3"
    ssid = true
    hide_missing = true
    hide_inactive = true
    speed_up = false
    speed_down = false

    [[block]]
    block = "net"
    device = "enp0s13f0u3u1"
    hide_missing = true
    hide_inactive = true
    speed_up = false
    speed_down = false

    [[block]]
    block = "battery"
    interval = 10
    format = "{percentage}%"

    [[block]]
    block = "sound"
    step_width = 3
    
    [[block]]
    block = "time"
    interval = 60
    format = "%a %v %R"
    '';

}
