{ config, pkgs, ... }:

  let
    pkgs-unstable = import <nixos-unstable> {};
  in
{
  imports = [
    ./wsl.nix
  ];

  home.packages = [ 
  ];
}
