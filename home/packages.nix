{ config, pkgs, ... }:

let
	nixpkgs-unstable = import <nixpkgs-unstable> {};
in
{
	imports = [
#		./home-secret.nix
		./paraview.nix
	];
	programs.home-manager.enable = true;
	
	home.packages = with pkgs; [
		htop
		ctags
		unzip
		git-crypt
		conda
		fzf
		sshfs-fuse
                texstudio
	];
	
	programs.git = {
		enable = true;
	};
	
	programs.ssh = {
		enable = true;
	};

	programs.bash = {
		enable = true;
		shellAliases = {
			cp = "rsync -ah --inplace --info=progress2";
			gvim = "vim -g";
                        icat = "kitty +kitten icat";
		};
	};
}
		
	
